package com.levi.sempreanote.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Lembrete {

	@Id
	@GeneratedValue
	private Long idLembrete;

	@ManyToOne
	@JoinColumn(name = "id_caderno")
	@NotNull(message="Caderno obrigatório!")
	private Caderno caderno;

	@NotEmpty(message="Titulo obrigatório!")
	private String titulo;
	
	@NotEmpty(message="Detalhes obrigatório")
	private String detalhes;


	public Long getIdLembrete() {
		return idLembrete;
	}

	public void setIdLembrete(Long idLembrete) {
		this.idLembrete = idLembrete;
	}

	public Caderno getCaderno() {
		return caderno;
	}

	public void setCaderno(Caderno caderno) {
		this.caderno = caderno;
	}
	

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public String getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}
	
	

}
