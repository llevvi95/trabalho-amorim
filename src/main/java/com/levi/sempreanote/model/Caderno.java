package com.levi.sempreanote.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Caderno {
	@Id
	@GeneratedValue
	@Column(name="id_caderno")
	private Long idCaderno;
	
	@NotEmpty(message="Nome obrigatório!")
	private String nome;
	
	private String descricao;
	
	@OneToMany(mappedBy = "caderno", cascade = CascadeType.ALL)
	private Set<Lembrete> lembretes;
	
	public Long getIdCaderno() {
		return idCaderno;
	}

	public void setIdCaderno(Long idCaderno) {
		this.idCaderno = idCaderno;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<Lembrete> getLembretes() {
		return lembretes;
	}

	public void setLembretes(Set<Lembrete> lembretes) {
		this.lembretes = lembretes;
	}

}
