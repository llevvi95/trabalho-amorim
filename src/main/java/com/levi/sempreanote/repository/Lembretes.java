package com.levi.sempreanote.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.levi.sempreanote.model.Lembrete;

public interface Lembretes extends JpaRepository<Lembrete, Long> {

}
