package com.levi.sempreanote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.levi.sempreanote.model.Caderno;

public interface Cadernos extends JpaRepository<Caderno, Long> {
	
	@Query(value = "select * from caderno c order by c.nome", nativeQuery = true)
	List<Caderno> findAllCadernosinOrder();

}
