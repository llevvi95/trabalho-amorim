package com.levi.sempreanote.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.levi.sempreanote.model.Caderno;
import com.levi.sempreanote.repository.Cadernos;

@Controller
@RequestMapping("/cadernos")
public class CadernosController {
	@Autowired
	private Cadernos cadernos;
	
	@GetMapping("")
	public ModelAndView listar() {
		ModelAndView mv = new ModelAndView("ListaCadernos");
		mv.addObject("cadernos",cadernos.findAll());
	return mv;
	}
	
	@RequestMapping("/novo")
	public ModelAndView novo(){
		ModelAndView mv = new ModelAndView("FrmCadernos");
		mv.addObject(new Caderno());
		return mv;
	}
	
	@PostMapping("")
	public ModelAndView salvar(@Validated Caderno caderno, Errors erros, RedirectAttributes redirectAttributes){
		ModelAndView mv = new ModelAndView("FrmCadernos");
		mv.addObject("cadernos", cadernos.findAll());
		if(erros.hasErrors()){
			return mv;
		}
		try{
			this.cadernos.save(caderno);
			return new ModelAndView("redirect:cadernos");
		}catch(Exception e){return mv;}
		
	}
	
	@RequestMapping(value ="/excluir/{idCaderno}")
	public String excluirCadernoByPathVariable(@PathVariable Long idCaderno, HttpServletRequest request, 
					HttpServletResponse response) {
		this.cadernos.delete(idCaderno);
		return "redirect:/cadernos";
	}
	
	@RequestMapping("/alterar/{idCaderno}")
	public ModelAndView alterarCadernoByPathVariable(@PathVariable Long idCaderno, HttpServletRequest request, 
			HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("FrmCadernos");
		mv.addObject("cadernos",cadernos.findAll());
		Caderno caderno = cadernos.findOne(idCaderno);
		mv.addObject(caderno);
		return mv;
	}
	
	@RequestMapping(value="{idCaderno}", method = RequestMethod.DELETE)
	public String excluir(@PathVariable Long idCaderno, RedirectAttributes attributes) {
		cadernos.delete(idCaderno);
		attributes.addFlashAttribute("mensagem", "Caderno excluído com sucesso!");
		return "redirect:/cadernos";
	}

}
