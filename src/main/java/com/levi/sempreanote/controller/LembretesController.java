package com.levi.sempreanote.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.levi.sempreanote.model.Caderno;
import com.levi.sempreanote.model.Lembrete;
import com.levi.sempreanote.repository.Cadernos;
import com.levi.sempreanote.repository.Lembretes;

@Controller
@RequestMapping("/lembretes")
public class LembretesController {
	
	@Autowired
	private Lembretes lembretes;
	@Autowired
	private Cadernos cadernos;

	@GetMapping("")
	public ModelAndView listar() {
		ModelAndView mv = new ModelAndView("LisLembrete");
		mv.addObject("lembretes",lembretes.findAll());
	return mv;
	}
	
	@RequestMapping("/novo")
	public ModelAndView novo(){
		ModelAndView mv = new ModelAndView("FrmLembrete");
		List<Caderno> allcadernos= cadernos.findAllCadernosinOrder();
		mv.addObject(new Lembrete());
		mv.addObject("allcadernos",allcadernos);
		return mv;
	}
	
	@PostMapping("")
	public ModelAndView salvar(@Validated Lembrete lembrete, Errors erros, RedirectAttributes redirectAttributes){
		ModelAndView mv = new ModelAndView("FrmLembrete");
		mv.addObject("lembretes", lembretes.findAll());
		List<Caderno> allcadernos= cadernos.findAllCadernosinOrder();
		mv.addObject("allcadernos",allcadernos);
		if(erros.hasErrors()){
			return mv;
		}
		try{
			this.lembretes.save(lembrete);
			return new ModelAndView("redirect:lembretes");
		}catch(Exception e){return mv;}
		
	}
	
	@RequestMapping(value ="/excluir/{idLembrete}")
	public String excluirLembreteByPathVariable(@PathVariable Long idLembrete, HttpServletRequest request, 
					HttpServletResponse response) {
		this.lembretes.delete(idLembrete);
		return "redirect:/lembretes";
	}
	
	@RequestMapping("/alterar/{idLembrete}")
	public ModelAndView alterarLembreteByPathVariable(@PathVariable Long idLembrete, HttpServletRequest request, 
			HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("FrmLembrete");
		List<Caderno> allcadernos= cadernos.findAllCadernosinOrder();
		mv.addObject("lembretes",lembretes.findAll());
		Lembrete lembrete = lembretes.findOne(idLembrete);
		mv.addObject("allcadernos",allcadernos);
		mv.addObject(lembrete);
		return mv;
	}
	
	@RequestMapping(value="{idLembrete}", method = RequestMethod.DELETE)
	public String excluir(@PathVariable Long idLembrete, RedirectAttributes attributes) {
		lembretes.delete(idLembrete);
		attributes.addFlashAttribute("mensagem", "Lembrete excluído com sucesso!");
		return "redirect:/lembretes";
	}

}
