package com.levi.sempreanote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SempreAnoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(SempreAnoteApplication.class, args);
	}
}
